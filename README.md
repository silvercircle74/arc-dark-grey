# Arc Theme - dark Grey variant

This is a fork of a fork of the popular ARC theme for GTK. It was forked from [this repository on 
Github](https://github.com/jnsh/arc-theme) and slightly modified to replace the blue-ish background with 
a neutral grey background. **Nothing else was changed, all instructions on how to build still apply**.

# TODO
Add screenshots

# Here is the original README

Arc is a flat theme with transparent elements for GTK 3, GTK 2 and various desktop shells, window 
managers and applications. It's well suited for GTK based desktop environments such as GNOME, Cinnamon, 
Xfce, Unity, MATE, Budgie etc.

The theme was originally designed and developed by [horst3180](https://github.com/horst3180/arc-theme), 
but the project has been unmaintained since May 2017.

This fork aims to keep the theme updated with new toolkit and desktop environment versions, resolve 
pre-existing issues, and improve and polish the theme while preserving the original visual design.

## Arc is available in four variants

##### Arc

## Supported toolkits and desktops

Arc comes with themes for the following:
* GTK 2
* GTK 3
* GTK 4
* GNOME Shell >=3.28
* Cinnamon >=3.8
* Unity
* Metacity
* Xfwm
* Plank

## Installation

#### Packages

Arc Theme is available from the official software package repositories for many common Linux 
distributions and FOSS operating systems. Installing the theme by using the package manager of your 
operating system should be the preferred method in most cases.

See [the wiki](https://github.com/jnsh/arc-theme/wiki/Packages) for non-exhaustive list of distribution packages, and some additional packages.

#### Manual installation

For installing the theme by compiling it from the source code, see 
[INSTALL.md](https://github.com/jnsh/arc-theme/blob/master/INSTALL.md) for build instructions, list of 
dependencies, build options and additional details.

## Issues

If you are experiencing any kind of issues, found a bug, or have a suggestion for improving the theme, 
please open an issue at <https://github.com/jnsh/arc-theme/issues>.

Some known issues are addressed in [common problems](https://github.com/jnsh/arc-theme/wiki/Common-problems).

## Contributing

If you'd like to contribute to the project, open an pull request, or an issue for discussion. See 
[HACKING.md](https://github.com/jnsh/arc-theme/blob/master/HACKING.md) for further details.

## License

Arc is available under the terms of the GPL-3.0. See [COPYING](https://github.com/jnsh/arc-theme/blob/master/COPYING) for details.

